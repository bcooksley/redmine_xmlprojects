require 'builder'

class XmlGenerate
  def self.generate_tree( file )
      @projectxml = File.open(file, "w")
      xml = Builder::XmlMarkup.new(:target => @projectxml, :indent => 2)
      xml.instruct!

      # Find components and iterate...
      xml.kdeprojects :version => 1 do
          toplevel = Project.find( :all, :conditions => { :parent_id => nil } )
          toplevel.each do |component|
              build_project( component, 'component', xml )
          end
      end
      @projectxml.close
  end
  
  private
  
  def self.build_project( project, rootname, xml )
    # Prepare to generate.....
    children = Project.find( :all, :conditions => { :parent_id => project.id } )
    project_path = project.to_a.to_param
    project_url = "http://projects.kde.org/projects/" + project_path
    
    # If it is archived -> ignore
    if project.status == Project::STATUS_ARCHIVED
      return
    end
    
    xml.tag!(rootname.to_sym, :identifier => project.identifier) do
      xml.name         project.name
      xml.description  do
        xml.cdata!( project.description.chomp )
      end
      xml.path         project_path
      xml.web          project_url
      
      if project.repository
        xml.icon         project.repository.xmlproject_icon
        build_repository( project, xml )
      end
      
      project.members.each do |member|
        xml.member    member.name, :username => member.user.login
      end

      children.each do |child|
        nextname = item_name( rootname )
        build_project( child, nextname, xml )
      end
    end

  end
  
  def self.item_name( old_name )
    namemap = { "component" => "module", "module" => "project", "project" => "project" }
    return namemap[old_name]
  end
  
  def self.build_repository( project, xml )
    # Build the urls...
    repository = project.repository
    gitweb_url = "http://gitweb.kde.org/" + project.identifier + ".git"
    projects_url = "http://projects.kde.org/projects/" + project.to_a.to_param + "/repository"
    
    # Build branch list + options
    branches = Hash[ *repository.branches.collect{ |b| [b,{}] }.flatten ]
    branches.delete( repository.xmlproject_i18n_trunk )
    branches.delete( repository.xmlproject_i18n_stable )
    branches.delete( repository.xmlproject_i18n_trunk_kf5 )
    branches.delete( repository.xmlproject_i18n_stable_kf5 )

    xml.repo do
      xml.web          projects_url, :type => "projects"
      xml.web          gitweb_url, :type => "gitweb"
      repository.checkout_protocols.each do |entry|
        xml.url        entry.url, :access => entry.access_rw(User.anonymous), :protocol => entry.protocol.downcase
      end
      branches.sort.each do |branch, options|
        xml.branch     branch, options
      end
      xml.branch       repository.xmlproject_i18n_trunk, :i18n => "trunk"
      xml.branch       repository.xmlproject_i18n_stable, :i18n => "stable"
      xml.branch       repository.xmlproject_i18n_trunk_kf5, :i18n => "trunk_kf5"
      xml.branch       repository.xmlproject_i18n_stable_kf5, :i18n => "stable_kf5"
      xml.active       integer_to_string_boolean( repository.xmlproject_active )
    end
  end
  
  def self.integer_to_string_boolean( value )
    return "true" if value == "1"
    return "false"
  end

end
