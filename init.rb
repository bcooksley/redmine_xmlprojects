require 'redmine'

require 'dispatcher'
Dispatcher.to_prepare do
  # Patches
  require_dependency 'xmlprojects/repository_patch'
  require_dependency 'xmlprojects/repositories_helper_patch'
end

Redmine::Plugin.register :redmine_xmlprojects do
  name 'Redmine Projects XML output'
  author 'Ben Cooksley'
  description 'Provides an interface to manage settings and generate a XML file of the projects'
  version '0.0.1'
  url 'http://projects.kde.org/projects/websites/redmine_xmlprojects'
  author_url ''
end
