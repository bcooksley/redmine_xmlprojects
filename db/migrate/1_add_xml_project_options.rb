class AddXmlProjectOptions < ActiveRecord::Migration
  def self.up    
    add_column :repositories, :xmlproject_settings, :text
  end
  
  def self.down    
    remove_column :repositories, :xmlproject_settings
  end
end
