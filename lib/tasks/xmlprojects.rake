namespace :xmlprojects do
  desc "Generate a KDE Projects XML file for use by external agents."
  task :build_xml => :environment do
    file = ENV['file']
    file ||= ENV['FILE']
    XmlGenerate.generate_tree(file)
  end
end