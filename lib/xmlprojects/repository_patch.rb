require_dependency 'repository'

module XmlProjects
  module RepositoryPatch
    def self.included(base) # :nodoc:
      base.send(:include, InstanceMethods)
      
      base.class_eval do
        unloadable
        serialize :xmlproject_settings, Hash
      end

      # Readers and Writers will be automatically defined for those listed in varlist
      # The data will be read/written to Repository.xmlproject_settings
      varlist = ['i18n_trunk', 'i18n_stable', 'i18n_trunk_kf5', 'i18n_stable_kf5', 'icon', 'active']
      varlist.each do |var|
          read_method = "def xmlproject_#{var}\nxmlproject_settings['#{var}']\nend"
          write_method = "def xmlproject_#{var}=(value)\nxmlproject_settings['#{var}'] = value\nend"
          base.class_eval read_method, "generated code (#{__FILE__}:#{__LINE__})"
          base.class_eval write_method, "generated code (#{__FILE__}:#{__LINE__})"
      end
    end
    
    module InstanceMethods
      def after_initialize
        self.xmlproject_settings ||= {}
        super
      end
      
      def buildsystems
        ["CMake", "Qt", "no-build", "Other"]
      end
    end

  end
end
Repository.send(:include, XmlProjects::RepositoryPatch)

Redmine::Scm::Base.all.each do |scm|
  require_dependency "repository/#{scm.underscore}"
  cls = Repository.const_get(scm)
  
  class_mod = Module.new
  class_mod.module_eval(<<-EOF
    def self.included(base)
      base.class_eval do
        unloadable
        serialize :xmlproject_settings, Hash
      end
    end
  EOF
  )
  cls.send(:include, class_mod)
end
