require_dependency 'repositories_helper'

module XmlProjects
  module RepositoriesHelperPatch
    def self.included(base) # :nodoc:
      base.send(:include, InstanceMethods)

      base.class_eval do
        alias_method_chain :repository_field_tags, :xmlprojects
      end
    end
  
    module InstanceMethods
      def repository_field_tags_with_xmlprojects(form, repository)    
        tags = repository_field_tags_without_xmlprojects(form, repository) || ""
        return tags if repository.class.name == "Repository"
      
        tags + @controller.send(:render_to_string, :partial => 'projects/settings/repository_xmlprojects', :locals => {:form => form, :repository => repository, :xmlsettings => repository.xmlproject_settings})
      end
    end
    
  end
end

RepositoriesHelper.send(:include, XmlProjects::RepositoriesHelperPatch)

